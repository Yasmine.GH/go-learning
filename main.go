package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

func f() {
	fmt.Println("your time is up")
	os.Exit(0)
}

func main() {

	var questionsNumber, correctAnswers int
	flag.Parse()

	var fileName string = "problem.csv"
	var timerDuration int = 30

	if flag.Arg(0) != "" && flag.Arg(1) != "" {
		var arg int
		fmt.Sscan(flag.Arg(0), &arg)

		if arg == 0 {
			fmt.Sscan(flag.Arg(1), timerDuration)
			fileName = flag.Arg(0)
		} else {
			timerDuration = arg
			fileName = flag.Arg(1)
		}

	} else if flag.Arg(0) != "" && flag.Arg(1) == "" {
		var arg int
		fmt.Sscan(flag.Arg(0), &arg)

		if arg == 0 {
			fileName = flag.Arg(0)
		} else {
			timerDuration = arg
		}
	}

	fmt.Printf("duration %v  filename %v \n", timerDuration, fileName)

	fmt.Print("Hello, Let's do some math!\n  Are you ready to start? (y/n) ")
	var start string
	fmt.Scan(&start)
	if start == "n" {
		fmt.Print("Next time then!\n")
		return
	}

	DurationOfTime := time.Duration(30) * time.Second
	Timer1 := time.AfterFunc(DurationOfTime, f)

	defer Timer1.Stop()

	f, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)

	for {
		questionsNumber++
		rec, err := csvReader.Read()
		if err == io.EOF {
			questionsNumber--
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		var answer int

	goBackHere:
		for {

			fmt.Printf("%s = ", rec[0])
			_, e := fmt.Scan(&answer)
			if e == nil {

				break goBackHere

			} else {
				fmt.Println("You need to enter a number")
			}
		}
		var correctAnswer int
		fmt.Sscan(rec[1], &correctAnswer)
		if answer == correctAnswer {
			correctAnswers++
		}
	}
	fmt.Printf("You have %v correct Answer out of %v questions", correctAnswers, questionsNumber)

}
